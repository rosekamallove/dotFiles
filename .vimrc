"-- Main Setting -------------------------------------------------------------------------------
au FocusGained,BufEnter * :checktime "File Update automatically 
"set spell spelllang=en_us
set relativenumber
set number
set encoding=utf-8
set cursorline
set laststatus=1 
set ruler
set mouse=a       "enable mouse use
set smartindent
set incsearch     "search as chars are entered
set hlsearch      "highlight matching searches
set path=.,,**
set smarttab
set showtabline=2  " Tabline
set updatetime=50
set timeoutlen=300
set wildmenu      " Tab autocomplete in command mode
set noshowmode    " No insert/normal indicator
set showcmd       " Show size of visual selection

" -- Searching -----------------------------------------------
set ignorecase    " Ignore case
set smartcase     " Don't ignore case if uppercase letter present

" -- BetterLineWrapping --------------------------------------
set nowrap
set linebreak
set textwidth=0 
set wrapmargin=0

" -- GitBranchOnStatusLine -------------------------------------
let gitBranch=system("git rev-parse --abbrev-ref HEAD")
set laststatus=2
set statusline=%F%m%r%h%w\ [%p%%]\ [LEN=%L]\ 
execute "set statusline +=" . gitBranch


" -- VimPluginsUsingPlug ---------------------------------------
call plug#begin()

" -- WebDev ------------------
Plug 'gko/vim-coloresque'
Plug 'airblade/vim-gitgutter'
Plug 'mattn/emmet-vim'

" -- LooksAndFeel ------------
Plug 'ryanoasis/vim-devicons'
Plug 'dense-analysis/ale'
Plug 'airblade/vim-rooter'
Plug 'itchyny/lightline.vim'

"-- MarkDown -------------------------------------------------------------------------
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
Plug 'godlygeek/tabular' 
Plug 'tpope/vim-markdown'

" -- Coc ---------------------
Plug 'neoclide/coc.nvim',   {'branch':'release'}

" -- Productivity ------------
Plug 'ctrlpvim/ctrlp.vim'
Plug 'djoshea/vim-autoread' "Reload File
Plug 'Yggdroot/indentLine'
Plug 'ervandew/supertab'
Plug 'wakatime/vim-wakatime'
Plug 'voldikss/vim-floaterm'
Plug 'tpope/vim-fugitive'
Plug 'diepm/vim-rest-console' " Rest Client 
Plug 'mcchrish/nnn.vim'


" -- SyntaxAndColorSchemes ---- 
Plug 'sheerun/vim-polyglot'
Plug 'sainnhe/gruvbox-material'
Plug 'morhetz/gruvbox'
Plug 'sainnhe/everforest'
Plug 'joshdick/onedark.vim'
Plug 'lifepillar/vim-solarized8'
Plug 'shinchu/lightline-gruvbox.vim'

" -- CustomOperators ------------
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'christoomey/vim-system-copy'

call plug#end()

autocmd VImEnter *
			\ if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
			\|   PlugInstall --sync | q
			\|endif
let g:autoclose_on=0

" -- ColorScheme -------------------------------------------------
let g:lightline = {'colorscheme':'gruvbox'} 
nnoremap LIT :colorscheme 
      \solarized8
      \<bar> set background=light 
      \<CR>
nnoremap SO :so ~/.vimrc<CR>
set termguicolors
let g:gruvbox_material_background = 'hard'
let g:gruvbox_material_palette = 'mix'
let g:everforest_background = 'hard'
let g:gruvbox_contrast_dark = 'hard'
let ayucolor="dark"
set background=dark
colorscheme gruvbox

" -- GitFugitive -----------------------------
nmap <Space>gh :diffget //3<CR>
nmap <Space>gs :diffget //2<CR>
nmap <Space>gs :G<CR>


" -- KeyBindingsForResizingSplits -----------------------------
nnoremap <C-Up> :resize +2<CR> 
nnoremap <C-Down> :resize -2<CR>
nnoremap <C-Left> :vertical resize +2<CR>
nnoremap <C-Right> :vertical resize -2<CR>

" -- TabToNextBuffer ------------------------------------------
nnoremap <TAB> :tabnext<CR>
nnoremap <S-TAB> :tabprevious<CR>

" -- BetterTabbing --------------------------------------------
vnoremap < <gv
vnoremap > >gv

" -- KeyMaps --------------------------------------------------
:imap kj <Esc>
noremap ter  :bot terminal<CR>
noremap splr :botright vert split<CR>
noremap spl  :split<CR>
nnoremap FF :%!astyle<CR><C-o>

" -- Window Nav -----------------------------------------------
nnoremap <C-H> <C-w>h
nnoremap <C-J> <C-w>j
nnoremap <C-K> <C-w>k
nnoremap <C-L> <C-w>l

" -- CPP Snippets ---------------------------------------------
set autoread 
au CursorHold * checktime

noremap cpp :0r ~/.vim/templates/basic.cpp<CR>
noremap md :0r ~/.vim/templates/md.md<CR>
noremap ccpp :0r ~/.vim/templates/code.cpp <bar> :28 <CR> 
map <C-a> <esc>ggVGcp<CR>

nnoremap cpb :
			\!clear;
			\cpb test %<CR>

noremap <F4> :
			\!clear; 
			\echo "-------------------------";
			\echo "$(tput bold)Compiling";
			\echo "-------------------------";
			\g++ -Wall -std=c++17 % -o .a.out; 
			\echo -e "\e[1;32m-------------------------";
			\echo -e "$(tput bold)Running";
			\echo -e "-------------------------\e[0m";
			\./.a.out; 
			\echo "\n";
			\echo "-------------------------";
			\echo "Debug:";
			\echo "-------------------------";
			\cat .deb.txt
			\<CR>

noremap <F3> :
			\!clear;
			\ echo "-------------------------";
			\ echo "$(tput bold)Compiling";
			\ echo "-------------------------";
			\g++  -Wall -std=c++17 % -o .Z.out;
			\./.Z.out < .zin.txt > .zot.md;
			\diff .zex.txt .zot.md > .zdf.txt<CR>

" -- IndentLines -------------------------------------------------
let g:indentLine_char = '|'
let g:indentLine_first_char = '|'
let g:indentLine_showFirstIndentLevel = 1
let g:indentLine_setColors = 0

" -- AleConfig -------------------------------------------------------------
let g:ale_disable_lsp = 1
let g:ale_sign_column_always = 0
let g:ale_sign_error = ''
let g:ale_sign_warning = ''

" -- COCConfig -------------------------------------------------------------
let g:coc_global_extensions = [
			\ 'coc-snippets',
			\ 'coc-pairs',
			\ 'coc-tsserver',
     	\ 'coc-deno',
			\ 'coc-python',
			\ 'coc-eslint',
			\ 'coc-prettier',
			\ 'coc-json',
			\ 'coc-html',
			\ 'coc-css',
			\ 'coc-tailwindcss',
			\ 'coc-go',
      \ 'coc-explorer',
			\ ]

"-- COC Exploerer Settings ------------------------------
let g:coc_explorer_global_presets = {
\   '.vim': {
\     'root-uri': '~/.vim',
\   },
\   'tab': {
\     'position': 'tab',
\     'quit-on-open': v:true,
\   },
\   'floating': {
\     'position': 'floating',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingTop': {
\     'position': 'floating',
\     'floating-position': 'center-top',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingLeftside': {
\     'position': 'floating',
\     'floating-position': 'left-center',
\     'floating-width': 50,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingRightside': {
\     'position': 'floating',
\     'floating-position': 'right-center',
\     'floating-width': 50,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'simplify': {
\     'file-child-template': '[selection | clip | 1] [indent][icon | 1] [filename omitCenter 1]'
\   }
\ }
nmap <space>e :CocCommand explorer<CR>
nmap <space>f :CocCommand explorer --preset floating<CR>
autocmd BufEnter * if (winnr("$") == 1 && &filetype == 'coc-explorer') | q | endif

autocmd FileType cpp let b:coc_pairs_disabled = ["<"]

au BufNewFile,BufRead *.js,*.css,*.md,*.ts
			\ set tabstop=2 |
			\ set softtabstop=2 |
			\ set shiftwidth=2 |
			\ set textwidth=120 |
			\ set expandtab |
			\ set autoindent |
			\ set fileformat=unix |

au BufNewFile,BufRead *.cpp,*.py,*.html
			\ set tabstop=4 |
			\ set softtabstop=4 |
			\ set shiftwidth=4 |
			\ set textwidth=120 |
			\ set expandtab |
			\ set autoindent |
			\ set fileformat=unix |

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
	if (index(['vim','help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	else
		call CocAction('doHover')
	endif
endfunction

nmap <F2> <Plug>(coc-rename)


" -- CTRLPConfig ----------------------------------------------------------
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
			\ 'dir':  '\v[\/]\.(git|hg|svn)$',
			\ 'file': '\v\.(exe|so|dll)$',
			\ 'link': 'some_bad_symbolic_links',
			\ }

" -- NerdTree ----------------------------------------------------------------
let g:nerdtree_tabs_open_on_console_startup = 2
let NERDTreeShowHidden=1
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
"map <space>e <plug>NERDTreeTabsToggle<CR>


"-- Save Text Folding --------------------------------------------------------
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview

" -- FloatTem ----------------------------------------------------------------
let g:floaterm_keymap_toggle = '<F12>'

" -- Mode Settings -----------------------------------------------------------
"Mode Settings
let &t_SI.="\e[6 q" "SI = INSERT mode
let &t_SR.="\e[4 q" "SR = REPLACE mode
let &t_EI.="\e[2 q" "EI = NORMAL mode (ELSE)
"Cursor settings:
"  1 -> blinking block
"  2 -> solid block 
"  3 -> blinking underscore
"  4 -> solid underscore
"  5 -> blinking vertical bar
"  6 -> solid vertical bar

" -- MarkDown --------------------------------------------------------------------------------
nnoremap <F9> :MarkdownPreviewToggle<CR>
let g:mkdp_refresh_slow=1

let g:indentLine_setConceal = 0
" default ''.
" n for Normal mode
" v for Visual mode
" i for Insert mode
" c for Command line editing, for 'incsearch'
let g:indentLine_concealcursor = ""
